import AsyncStorage from '@react-native-community/async-storage';
import React, { useEffect, useState, useRef } from 'react'
import { StyleSheet, Image, View, Text, TextInput, Modal, ImageBackground } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Button } from '../../../components/Button';
import { RNCamera } from 'react-native-camera'
import colors from '../../../styles/colors'
import styles, {styling} from '../../../styles/style'
import Axios from 'axios';
import api from '../../../api'

const UpdateProfile = ({route, navigation}) => {
    const props = route.params
    let camera = useRef(null)
    const [input, setInput] = useState('')
    const [isEditable, setIsEditable] = useState(false)
    const [token, setToken] = useState('')
    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const toogleCamera = () => {
        setType(type === 'back' ? 'front' : 'back')
    }

    const takePicture = async() =>{
        const options = { quality: 0.5, base64: true }
        if(camera){
            const data = await camera.current.takePictureAsync(options)
            setPhoto(data)
            setIsVisible(false)
        }
    }

    useEffect(() => {
        const getToken = async() => {
            try{
                const token = await AsyncStorage.getItem('token')
                if(token !== null){
                    setToken(token)
                }
            }catch(err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const renderCamera = () => {
        return(
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{flex:1}}>
                    <RNCamera
                        style={{flex: 1, flexDirection: 'column-reverse', paddingBottom: 32}}
                        type={type}
                        ref={camera}>
                        <View style={updateStyles.camera}>
                            <Button onPress={() => {takePicture()}}>
                                <Icon name="camera" size={50} color={colors.lightGrey} />
                            </Button>
                            <Button onPress={() => {toogleCamera()}} style={{marginLeft: 32}}>
                                <Icon name="sync-alt" size={15} color={colors.lightGrey} />
                            </Button>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    const handleEditable = () => {
        setInput(props.name)
        setIsEditable(!isEditable)
    }

    const handleSavePress = () => {
        const formData = new FormData()
        formData.append('name', input)
        formData.append('photo', {
            uri: photo.uri,
            name: 'photo.jpg',
            type: 'image/jpg',
        })
        Axios.post(`${api}/profile/update-profile`, formData, {
            timeout: 20000,
            headers: {
                'Authorization' : 'Bearer' + token,
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data'
            }
        })
        .then((res) => {
            console.log('final res -> ', res)
            navigation.navigate("Profile")
        }).catch(err => {
            console.log(err)
            alert("Update Failed")
            navigation.navigate("Profile")
        })
    }

    return(
        <View style={updateStyles.container}>
            <ImageBackground
                imageStyle={{borderRadius: 80}}
                source={photo === null ? {uri: props.photo} : {uri: photo.uri}}
                style={updateStyles.imgContainer}>
                <TouchableOpacity 
                    onPress={() => {setIsVisible(true)}}>
                    <Icon name="camera" size={25} color={colors.lightGrey} style={{opacity: 0.5}} />
                </TouchableOpacity>
            </ImageBackground>
            <View style={updateStyles.descContainer}>
                <View style={updateStyles.itemDesc}>
                    <Text style={updateStyles.textDesc}>Name : </Text>
                    {
                        isEditable === true ? (<TextInput 
                            style={{marginHorizontal: 16, backgroundColor: colors.white, width: '60%', borderRadius: 6, padding: 8}}
                            value={input}
                            onChangeText={(val) => {setInput(val)}} />
                        ) : (<Text style={updateStyles.textDesc, {marginHorizontal: 16}}>{props.name}</Text>)
                    }
                    <Icon style={{marginLeft: 16}} onPress={handleEditable} name="edit" size={20} color={colors.darkYellow} />
                </View>
                <View style={updateStyles.itemDesc}>
                    <Text style={updateStyles.textDesc}>Email : </Text>
                    <Text style={updateStyles.textDesc, {marginHorizontal: 16}}>{props.email}</Text>
                    <Text></Text>
                </View>
                <View style={{width: '90%', alignItems: 'center', marginTop: 16}}>
                    <Button
                        onPress={() => {handleSavePress()}}
                        style={{...styles.btnRegister, borderColor: colors.lightBlue}} >
                        <Text style={styles.btnTextRegister, {color: colors.lightBlue}}>Submit Update</Text>
                    </Button>
                </View>
            </View>
            {renderCamera()}
        </View>
    )
}

const updateStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlue,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imgContainer: {
        width: 120, 
        height: 120,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    descContainer: {
        width: '90%', 
        padding: 8,
        margin: 16,
        alignItems: 'center', 
        backgroundColor: colors.white,
        borderRadius: 6
    },
    itemDesc: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '90%'  
    },
    textDesc: {
        fontSize: 16,
        paddingVertical: 16,
        color: colors.lightBlue
    },
    camera: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }

})

export default UpdateProfile