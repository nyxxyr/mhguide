import React from 'react'
import {View, Text, Image} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {styling} from '../../styles/style'

const User = ({profile, navigation}) => {
    
    const handleBtnUpdate = () => {
        navigation.navigate("Edit", profile)
    }

    return(
        <>
        <View style={{paddingBottom: 10}}>
            <Text style={[styling.bgBlue, styling.title, {color: '#fff', fontWeight: '500', padding: 16, fontSize: 20}]}>Account</Text>
            <TouchableOpacity 
                onPress={() => {handleBtnUpdate()}}
                style={[styling.flexRow, styling.userName]}>
                <Image 
                    style={styling.photoUser}
                    source={{uri: profile.photo}} 
                />
                <Text style={[styling.title, {fontSize:16}]}>{profile.name}</Text>
            </TouchableOpacity>
        </View>
        </>
    )
}

export default User