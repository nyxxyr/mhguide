import React from 'react'
import {View, Text} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import {styling} from '../../styles/style'
import colors from '../../styles/colors'

const Menu = ({icon, desc}) => {
    return(
        <>
            <View>
                <View style={[styling.flexRow, {paddingHorizontal: 16, paddingVertical: 10}]}>
                    <View>
                        <Icon name={icon} size={30} color={colors.lightBlue} />
                    </View>
                    <View style={[styling.flexRow, styling.itemDesc, {flexGrow: 1}]}>
                        <Text style={{color: colors.darkGreen}}>{desc}</Text>
                    </View>
                </View>
            </View>
        </>
    )
}

export default Menu