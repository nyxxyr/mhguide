import React, {useEffect, useState} from 'react'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import api from '../../api'
import {styling} from '../../styles/style'
import colors from '../../styles/colors'
import { Text, View, StatusBar, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Menu from './Menu'
import User from './User'
import { GoogleSignin } from '@react-native-community/google-signin'


const Profile = ({navigation}) => {
    const [profile, setProfile] = useState("")
    const [token, setToken] = useState('')

    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem('token')
                setToken(token)
                if(token !== null){
                    return getVenue(token)
                }else{
                    getCurrentUser()
                }
            }catch (err){
                console.log('errToken -> ',err)
            }
        }
        getToken()
    }, [])

    const getCurrentUser = async () => {
        try{
            const user = await GoogleSignin.signInSilently()
            let data = user.user
            setProfile({
                name: `${data.givenName} ${data.familyName}`,
                photo: data.photo
            })
        }catch(err){
            console.log('err current user -> '. err)
        }

    }

    const getVenue = (token) => {
            Axios.get(`${api}/profile/get-profile`, {
                timeout: 20000,
                headers: {
                    'Authorization':  'Bearer' + token  
                }
            })
            .then((res) => {
                let data = res.data.data.profile
                setProfile({
                    name: data.name,
                    photo: `https://crowdfunding.sanberdev.com${data.photo}`,
                    email: data.email
                })
            }).catch(err => {
                console.log('errVenue -> ', err)
            })
    }

    const handleLogout = async () => {
        try{
            if(token === null){
                try{
                    await GoogleSignin.revokeAccess()
                    await GoogleSignin.signOut()
                }catch(err){
                    console.log(err)
                }
            }else{
                await AsyncStorage.removeItem('token')
            }
            navigation.navigate('Login')
        }catch (err){
            console.log(err)
        }
    }

    return(
        <View>
            <StatusBar barStyle="light-content" backgroundColor={colors.lightBlue}></StatusBar>
            <User profile={profile} navigation={navigation} />
            <View style={[styling.flexRow, {paddingHorizontal: 16, paddingVertical: 10}]}>
                <View>
                    <Icon name="wallet" size={30} color={colors.lightBlue} />
                </View>
                <View style={[styling.flexRow, styling.itemDesc, {flexGrow: 1}]}>
                    <Text style={{color: colors.darkGreen}}>Saldo</Text>
                    <Text style={{color: colors.darkGreen}}>Rp. 40.000</Text>
                </View>
            </View>
            <View style={{marginTop: 16}}>
                <Menu icon="tools" desc="Pengaturan" />
                <Menu icon="question-circle" desc="Bantuan" />
                <Menu icon="file-alt" desc="Syarat dan Ketentuan" />
            </View>
            <TouchableOpacity
                onPress={() => {handleLogout()}} 
                style={[styling.flexRow, {paddingHorizontal: 16, paddingVertical: 10, marginTop: 16}]}>
                <View>
                    <Icon name="sign-out-alt" size={30} color={colors.lightBlue} />
                </View>
                <View style={[styling.flexRow, styling.itemDesc, {flexGrow: 1}]}>
                    <Text style={{color: colors.darkGreen}}>Keluar</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default Profile