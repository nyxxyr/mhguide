import React, { useState } from 'react'
import {View, Text, TextInput, StyleSheet} from 'react-native'
import api from '../../api'
import { Button, LinkBtn } from '../../components/Button'
import colors from '../../styles/colors'
import styles from '../../styles/style'
import Axios from 'axios'

const Register = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [name, setName] = useState('')

    const handlehaveAcc = () => {
        navigation.navigate("Login")
    }

    const handleRegister = async() => {
        let data = {
            name: name,
            email: email
        }
        Axios.post(`${api}/auth/register`, data).then((res) => {
            alert("Berhasil Daftar, silahkan cek email untuk kode verifikasi")
            navigation.navigate("Verification", data)
        }).catch(err => {
            console.log("register -> err", err)
        })
    }

    return(
        <View style={RegisterStyles.registerContainer}>
            <View style={RegisterStyles.formCard}>
                <View>
                    <Text style={RegisterStyles.inputLabel}>Email atau No. Handphone</Text>
                    <View style={RegisterStyles.inputForm}>
                        <TextInput
                            onChangeText={(value) => {setEmail(value)}}
                            value={email}
                            style={{color: colors.darkGreen}}
                            placeholderTextColor={colors.darkGreen} />
                    </View>
                </View>
                <View>
                    <Text style={RegisterStyles.inputLabel}>Nama Lengkap</Text>
                    <View style={RegisterStyles.inputForm}>
                        <TextInput
                            onChangeText={(value) => {setName(value)}}
                            value={name}
                            style={{color: colors.darkGreen}}
                            placeholderTextColor={colors.darkGreen} />
                    </View>
                </View>
                <View style={{alignItems: 'center', paddingTop: 8}}>
                    <Button 
                        onPress={() => {handleRegister()}}
                        style={styles.btnLogin}>
                        <Text style={styles.btnTextLogin, {color: colors.darkGreen}}>Daftar</Text>
                    </Button>
                </View>
                <View style={RegisterStyles.haveAcc}>
                    <Text>Punya Akun ? </Text>
                    <LinkBtn
                        style={{marginLeft: 4}}
                        onPress={() => {handlehaveAcc()}} >
                        <Text style={{color: colors.darkGreen}}>Masuk</Text>
                    </LinkBtn>
                </View>
            </View>
            <View>
                <View style={{alignItems: 'center'}}>
                    <Text style={{color: colors.white}}>Pakai cara lain ?</Text>
                </View>
                <View style={RegisterStyles.otherOpt}>
                    <View style={{alignItems: 'center', width: '50%'}}>
                        <Button 
                            style={styles.btnLogin}>
                            <Text style={styles.btnTextLogin, {color: colors.darkGreen}}>Facebook</Text>
                        </Button>
                    </View>
                    <View style={{alignItems: 'center', width: '50%'}}>
                        <Button 
                            style={styles.btnRegister}>
                            <Text style={styles.btnTextRegister}>Google</Text>
                        </Button>
                    </View>
                </View>
            </View>
        </View>
    )
}

export const RegisterStyles = StyleSheet.create({
    registerContainer: {
        flex: 1,
        backgroundColor: colors.lightBlue,
        justifyContent: 'space-between'
    },
    formCard: {
        justifyContent: 'center',
        padding: 16
    },
    inputForm: {
        backgroundColor: colors.white,
        borderBottomColor: colors.darkGreen,
        borderBottomWidth: 1
    },
    inputLabel: {
        color: colors.white,
        paddingVertical: 8
    },
    haveAcc:{
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 8
    },
    otherOpt: {
        marginBottom: 32,
        marginHorizontal: 16,
        flexDirection: 'row',
        justifyContent: 'center'
    }
})

export default Register
