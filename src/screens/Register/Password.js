import React, { useState } from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'
import { Button } from '../../components/Button'
import Icon from 'react-native-vector-icons/FontAwesome5';
import colors from '../../styles/colors'
import styles from '../../styles/style'
import Axios from 'axios';
import api from '../../api';

const Password = ({route, navigation}) => {
    const props = route.params
    const [securePass, setSecurePass] = useState(true)
    const [securePassConfirm, setSecurePassConfirm] = useState(true)
    const [pass, setPass] = useState('')
    const [passConfirm, setPassConfirm] = useState('')

    const handleSave = () => {
        if(pass === passConfirm){
            let data = {
                password: pass,
                password_confirmation: passConfirm,
                email: props.email
            }
            Axios.post(`${api}/auth/update-password`, data).then(res => {
                alert("Akun siap digunakan")
                navigation.navigate("Login")
            }).catch(err => {
                console.log("password -> err ", err)
            })
        }else{
            alert("password dan konfirmasi password tidak cocok")
        }
    }

    return(
        <View style={passStyles.container}>
            <View style={passStyles.desc}>
                <Text style={passStyles.descText}>Masukkan kata sandi yang akan anda gunakan</Text>
            </View>
            <View style={{marginTop: 32}}>
                <View style={passStyles.passInput}>
                    <TextInput 
                        value={pass}
                        onChangeText={(val) => {setPass(val)}}
                        secureTextEntry={securePass}
                        placeholderTextColor={colors.darkGreen}
                        placeholder="Kata Sandi" />
                    <Button
                        onPress={() => {setSecurePass(!securePass)}} 
                        style={{marginRight: 16}}>
                        {
                            securePass ? 
                            (<Icon name="eye" size={20} color={colors.lightGrey} />) :
                            (<Icon name="eye-slash" size={20} color={colors.lightGrey} />)
                        }
                    </Button>
                </View>
                <View style={passStyles.passInput}>
                    <TextInput 
                        value={passConfirm}
                        onChangeText={(val) => {setPassConfirm(val)}}
                        secureTextEntry={securePassConfirm}
                        placeholderTextColor={colors.darkGreen}
                        placeholder="Konfirmasi Kata Sandi" />
                    <Button 
                        onPress={() => {setSecurePassConfirm(!securePassConfirm)}}
                        style={{marginRight: 16}}>
                        {
                           securePassConfirm ? 
                           (<Icon name="eye" size={20} color={colors.lightGrey} />) :
                           (<Icon name="eye-slash" size={20} color={colors.lightGrey} />) 
                        }
                    </Button>
                </View>
                <View style={{alignItems: 'center', marginTop: 16}}>
                    <Button 
                        onPress={() => {handleSave()}}
                        style={{...styles.btnLogin, height: 60}}>
                        <Text style={styles.btnTextLogin}>Simpan</Text>
                    </Button>
                </View>
            </View>
        </View>
    )
}

const passStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlue,
        justifyContent: 'flex-start',
    },
    desc: {
        marginHorizontal: 16,
        marginTop: 32
    },
    descText: {
        color: colors.white,
        fontSize: 16
    },
    passInput: {
        marginHorizontal: 16,
        borderColor: colors.darkGreen,
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between'
    }
})

export default Password