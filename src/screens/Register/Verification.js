import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import styles from '../../styles/style'
import colors from '../../styles/colors'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { Button, LinkBtn } from '../../components/Button'
import Axios from 'axios'
import api from '../../api'

const Verification = ({route, navigation}) => {
    const props = route.params
    const [code, setCode] = useState('')

    const handleVerification = () => {
        Axios.post(`${api}/auth/verification`, {code}).then(res => {
            alert("Verifikasi Berhasil")
            navigation.navigate("Password", props)
        }).catch(err => {
            console.log("verifikasi -> err ", err)
        })
    }

    return(
        <View style={verStyles.container}>
            <View style={{alignItems: 'center'}}>
                <View style={verStyles.desc}>
                    <Text style={verStyles.descText}>Masukkan 6 digit verifikasi yang telah kami kirimkan ke {props.email}</Text>
                </View>
                <View style={{height: '40%', width: '75%'}}>
                    <OTPInputView 
                        autoFocusOnLoad
                        codeInputFieldStyle={verStyles.underlineStyleBase}
                        codeInputHighlightStyle={verStyles.underlineStyleHighLighted}
                        placeholderTextColor={colors.darkGreen}
                        onCodeChanged={(kode) => {
                            setCode(kode)
                        }}
                        pinCount={6} />
                </View>
                <View style={{width: '75%', alignItems: 'center'}}>
                    <Button
                        onPress={() => {handleVerification()}} 
                        style={styles.btnLogin}>
                        <Text style={styles.btnTextLogin}>Verifikasi</Text>
                    </Button>
                    <LinkBtn 
                        style={{marginTop: 8}}>
                        <Text style={{color: colors.darkGreen}}>Resend Code</Text>
                    </LinkBtn>
                </View>
            </View>
        </View>
    )   
}

const verStyles = StyleSheet.create({
    container: {
        backgroundColor: colors.lightBlue,
        justifyContent: 'center',
        flex: 1
        
    },
    desc: {
        marginHorizontal: 16,
        marginTop: 8
    },
    descText: {
        color: colors.white,
        fontSize: 16
    },
    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
    },
    underlineStyleHighLighted: {
        borderColor: colors.darkGreen,
    },

})

export default Verification