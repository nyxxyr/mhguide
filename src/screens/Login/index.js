import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import api from '../../api'
import auth from '@react-native-firebase/auth'
import { StyleSheet, Text, View, TextInput } from 'react-native'
import {GoogleSignin, GoogleSigninButton, statusCodes} from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id'
import {Button} from '../../components/Button'
import styles from '../../styles/style'
import colors from '../../styles/colors'

const config = {
    title: 'Authentication Required',
    imageColor: colors.lightBlue,
    imageErrorColor: colors.mutedRed,
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Sensor Failed Recognize',
    cancelText: 'Cancel'
}

const Login = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const saveToken = async (token) => {
        try{
            await AsyncStorage.setItem('token', token)
        }catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '329413014153-88a78vv4n96kqo68csidfdl4ucu77gps.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try{
            const {idToken} = await GoogleSignin.signIn()

            const credential = auth.GoogleAuthProvider.credential(idToken)
            
            auth().signInWithCredential(credential)

            navigation.navigate('Profile')
        }catch(err){
            console.log('err -> ', err)
        }
    }

    const signinWithFinger = () => {
        TouchID.authenticate('', config)
            .then(success => {
                alert('Auth Success')
                navigation.navigate('Profile')
            })
            .catch(error => {
                alert('Auth Failed')
            })
    }

    const handleLogin = () => {
        let temp = {
            email: email,
            password: password
        }
        Axios.post(`${api}/auth/login`, temp, {
            timeout: 2000
        }).then(res => {
            saveToken(res.data.data.token)
            // console.log(res.data.data.token)
            setEmail('')
            setPassword('')
            navigation.navigate('Profile')  
        }).catch(err => {
            console.log(err)
        }) 
    }

    return(
        <>
        <View style={loginStyles.Container}>
            <View style={loginStyles.loginContainer}>
                <View style={{width: '90%', alignItems: 'center'}}>
                    <TextInput style={loginStyles.loginInput}
                        value={email}
                        onChangeText={(val) => setEmail(val)}
                        placeholder="Masukkan Email"
                        placeholderTextColor={colors.darkGreen} />
                    <TextInput style={loginStyles.loginInput}
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(val) => setPassword(val)}
                        placeholder="Masukkan Password"
                        placeholderTextColor={colors.darkGreen} />
                    <Button
                        onPress={() => {handleLogin()}}
                        style={styles.btnLogin}>
                        <Text style={styles.btnTextLogin}>Login</Text>
                    </Button>
                </View>
                <View style={{width: '90%', alignItems: 'center'}}>
                    <GoogleSigninButton
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Light}
                        onPress={signInWithGoogle} />
                    <View style={{width: '90%', alignItems: 'center'}}>
                        <Button
                            onPress={() => {signinWithFinger()}}
                            style={styles.btnLogin} >
                            <Text style={styles.btnTextLogin}>Sign in with fingerprint</Text>
                        </Button>
                    </View>
                </View>
            </View>
        </View>
        </>
    )
}

const loginStyles = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: colors.lightBlue,
        justifyContent: 'center',
    },
    loginContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    loginInput: {
        backgroundColor: colors.white,
        width: '90%',
        padding: 8,
        borderRadius: 6,
        marginTop: 16
    }
})

export default Login