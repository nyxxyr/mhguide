import React from 'react'
import { Image, SafeAreaView, StatusBar, Text, View } from 'react-native'
import styles from '../../styles/style'
import colors from '../../styles/colors'
import {Button} from '../../components/Button'
import AppIntroSlider from 'react-native-app-intro-slider'

const data = [
    {
        id: 0,
        image: require('../../assets/img/image1.png'),
        description: 'Barroth Hunt'
    },
    {
        id: 1,
        image: require('../../assets/img/image2.png'),
        description: 'Zorah Magdaros Arbalest'
    },
    {
        id: 2,
        image: require('../../assets/img/image3.png'),
        description: 'Zorah Magdaros Cannon'
    }
]

const Intro = ({navigation}) => {
    // tampilan yang ditampilkan dalam renderItem
    const renderItem = ({ item }) => {
        return(
            <View style={styles.listContainer}>
                <View style={styles.listContent}>
                    <Image source={item.image} style={styles.imgList} resizeMethod="auto" resizeMode="contain" />
                </View>
                <Text style={styles.textList}>{item.description}</Text>
            </View>
        )
    }

    return(
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <StatusBar backgroundColor={colors.lightBlue} barStyle="light-content" />
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>Monster Hunter Guide</Text>
                </View>
                <View style={styles.slider}>
                    {/* contoh menggunakan component react native app intro slider */}
                    <AppIntroSlider
                        data={data}
                        renderItem={renderItem}
                        renderNextButton={() => null}
                        renderDoneButton={() => null}
                        activeDotStyle={styles.activeDotStyle}
                        keyExtractor={(item) => item.id.toString()} />
                </View>
                <View style={styles.btnContainer}>
                    <Button style={styles.btnLogin}
                        onPress={() => navigation.navigate('Login')}>
                            <Text style={styles.btnTextLogin}>Masuk</Text>
                    </Button>
                    <Button style={styles.btnRegister}
                        onPress={() => navigation.navigate('Register')}>
                            <Text style={styles.btnTextRegister}>Daftar</Text>
                    </Button>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default Intro
