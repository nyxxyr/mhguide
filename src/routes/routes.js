import React, { useEffect, useState } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import SplashScreens from '../screens/SplashScreen'
import Intro from '../screens/Intro'
import Login from '../screens/Login'
import UpdateProfile from '../screens/Profile/Update'
import colors from '../styles/colors'
import Profile from '../screens/Profile'
import Register from '../screens/Register'
import Verification from '../screens/Register/Verification'
import Password from '../screens/Register/Password'


const Stack = createStackNavigator()

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen 
           name="Intro"
           component={Intro}
           options={{headerShown: false}} />
        <Stack.Screen 
           name="Login"
           component={Login}
           options={{headerStyle: {backgroundColor: colors.white}, headerTintColor: colors.darkGreen}} />
        <Stack.Screen
            name="Register"
            component={Register}
            options={{headerStyle: {backgroundColor: colors.white}, headerTintColor: colors.darkGreen}} />
        <Stack.Screen 
            name="Verification"
            component={Verification}
            options={{headerShown: false}} />
        <Stack.Screen 
            name="Password"
            component={Password}
            options={{headerShown: false}} />
        <Stack.Screen 
           name="Profile"
           component={Profile}
           options={{headerShown: false}} />
        <Stack.Screen 
           name="Edit"
           component={UpdateProfile}
           options={{headerStyle: {backgroundColor: colors.white}, headerTintColor: colors.darkGreen}} />
    </Stack.Navigator>
)

const AppNavigation = () => {
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)
    }, [])

    if(isLoading){
        return <SplashScreens />
    }

    return(
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation
