const colors = {
    lightBrown: '#af9483',
    darkGreen: '#006e51',
    lightBlue: '#92b6d5',
    darkYellow: '#d8ae47',
    mutedRed: '#ad5d5d',
    white : '#ffffff',
    blue: '#3a86ff',
    darkblue: '#088dc4',
    black: '#000000',
    grey: '#A0A0A0',
    lightGrey: '#e6e6e6',
    transparent: 'rgba(0, 0, 0, 0.5)'
}

export default colors