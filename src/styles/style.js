import { StyleSheet } from 'react-native'
import colors from '../styles/colors'

export const styling = StyleSheet.create({
    bgGreen: {
        backgroundColor: '#cee397'
    },
    photoUser: {
        width: 60,
        height: 60,
        borderRadius: 40,
        marginRight: 16
    }, 
    bgBlue: {
        backgroundColor: colors.lightBlue,
        padding: 10,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    flexRow: {
        flexDirection: 'row',
        alignItems: 'center',

    },
    itemDesc: {
        justifyContent: 'space-between',
        marginHorizontal: 8,
        paddingHorizontal: 8
    },
    userName: {
        padding: 16
    }
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.lightBlue
    },
    textLogoContainer: {
        marginTop: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textLogo: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.white
    },
    slider: {
        flex: 1
    },
    btnContainer: {
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    btnLogin: {
        height: 35,
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
        backgroundColor: colors.white
    },
    btnTextLogin: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.lightBlue
    },
    btnRegister: {
        height: 35,
        width: '90%',
        borderWidth: 1.5,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors.white,
        borderRadius: 6,
        backgroundColor: 'transparent'
    },
    btnTextRegister: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.white
    },
    listContainer: {
        marginTop: 25,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    listContent: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imgList: {
        width: 330,
        height: 330,
        borderRadius: 6,
        borderColor: colors.white
    },
    textList: {
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.white
    },
    activeDotStyle: {
        width: 20,
        backgroundColor: colors.white
    }
})

export default styles