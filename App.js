/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import AppNavigation from './src/routes/routes';
import firebase from '@react-native-firebase/app'

var firebaseConfig = {
  apiKey: "AIzaSyAE9Bmj0WJ3BlmrBhRkJrBonc_O93w8fE0",
  authDomain: "mhguide.firebaseapp.com",
  databaseURL: "https://mhguide.firebaseio.com",
  projectId: "mhguide",
  storageBucket: "mhguide.appspot.com",
  messagingSenderId: "329413014153",
  appId: "1:329413014153:web:db10bf14b240ec4a928c58",
  measurementId: "G-56FZFV0X7G"
};
// Initialize Firebase

if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}

const App: () => React$Node = () => {
  return (
    <>
      <AppNavigation />
    </>
  );
};

export default App;
